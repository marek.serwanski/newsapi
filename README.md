# Test Api News App

### Start app

As it is Spring Boot app, just start it. It has in-memory H2, default working params provided in application.yaml

### Save news from NewsApi.org

Send POST request to /newsapi with std json body like:

{"query": "world"}

For testing/presentation purposes, only query param is handled.

After successful response, DB is filled with data

### Get news from local app

Hit GET request to /getnews with optional requestParams offset and limit



