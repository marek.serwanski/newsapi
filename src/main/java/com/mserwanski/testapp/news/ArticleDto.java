package com.mserwanski.testapp.news;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
class ArticleDto {

    private String title;
    private String description;
    private Date publishDate;
}
