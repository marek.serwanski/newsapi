package com.mserwanski.testapp.news;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

import static io.micrometer.common.util.StringUtils.isNotBlank;

@Entity
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode
class ArticleEntity {

    @Id
    @GeneratedValue
    private int id;

    private String title;
    private String description;
    private Date publishDate;

    ArticleEntity(String title, String description, Date publishDate){
        this.title = truncate(title);
        this.description = truncate(description);
        this.publishDate = publishDate;
    }

    // dummy method only for helping local testing - some of newsApi articles are huuuuge
    private String truncate(String value) {
        return (isNotBlank(value) && value.length() > 200) ? value.substring(0, 199) : value;
    }
}
