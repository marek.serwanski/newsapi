package com.mserwanski.testapp.news;

import com.mserwanski.testapp.news.model.NewsApiRequest;
import com.mserwanski.testapp.news.model.NewsApiResponse;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

class NewsApiClient {

    private String key;
    private String url;

    private final RestTemplate restTemplate;

    NewsApiClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    NewsApiResponse getNewsApiResponce(NewsApiRequest request) {
        NewsApiResponse response = restTemplate.getForObject(getNewsApiUrl(request), NewsApiResponse.class);
        assert response != null && response.totalResults() != null;
        if (!response.status().equals("ok")) {
            throw new RestClientException("Bad response from newsApi.org. Status: " + response.status());
        }
        return response;
    }

    private String getNewsApiUrl(NewsApiRequest request) {
        return String.format(url, key, request.getPage()) + (request.getQuery() != null ? "&q=" + request.getQuery() : "");
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
