package com.mserwanski.testapp.news;

import org.modelmapper.ModelMapper;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableTransactionManagement
class NewsConfig {

    @Bean
    @ConfigurationProperties(prefix = "newsapi")
    NewsApiClient newsApiClient(RestTemplate restTemplate) {
        return new NewsApiClient(restTemplate);
    }

    @Bean
    NewsService newsApiService(NewsApiClient newsApiClient, NewsResource newsResource, ModelMapper modelMapper) {
        return new NewsService(newsApiClient, newsResource, modelMapper);
    }

    @Bean
    RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
