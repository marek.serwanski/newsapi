package com.mserwanski.testapp.news;

import com.mserwanski.testapp.news.model.NewsApiRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
class NewsEndpoint {

    private static final String DEFAULT_PAGE_SIZE = "10";
    private static final String DEFAULT_OFFSET = "0";

    private final NewsService newsService;

    NewsEndpoint(NewsService newsService) {
        this.newsService = newsService;
    }

    @GetMapping("/getnews")
    List<ArticleDto> getNews(@RequestParam(defaultValue = DEFAULT_PAGE_SIZE) Integer limit, @RequestParam(defaultValue = DEFAULT_OFFSET) Integer offset) {
        return newsService.getNews(offset, limit);
    }

    @PostMapping("/newsapi")
    void saveNewsFromNewsApi(@RequestBody NewsApiRequest request) {
        newsService.saveNewsFromNewsApi(request);
    }
}
