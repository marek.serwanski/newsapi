package com.mserwanski.testapp.news;

import jakarta.annotation.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

@Resource
interface NewsResource extends CrudRepository<ArticleEntity, Integer> {
    Page<ArticleEntity> findAllByOrderByPublishDateDesc(Pageable pageable);
}
