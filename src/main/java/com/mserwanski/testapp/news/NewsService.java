package com.mserwanski.testapp.news;

import com.mserwanski.testapp.news.model.Article;
import com.mserwanski.testapp.news.model.NewsApiRequest;
import com.mserwanski.testapp.news.model.NewsApiResponse;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;

import java.util.List;

class NewsService {

    private final NewsApiClient newsApiClient;
    private final NewsResource newsResource;
    private final ModelMapper modelMapper;

    NewsService(NewsApiClient newsApiClient, NewsResource newsResource, ModelMapper modelMapper) {
        this.newsApiClient = newsApiClient;
        this.newsResource = newsResource;
        this.modelMapper = modelMapper;
    }

    @Transactional
    public void saveNewsFromNewsApi(NewsApiRequest request) {
        NewsApiResponse response = newsApiClient.getNewsApiResponce(request);
        saveNewsInDB(response);
        if (moreNewsToBeSaved(request, response)) {
            saveNewsFromNewsApi(request.increasedPageRequest());
        }
    }

    List<ArticleDto> getNews(Integer offset, Integer limit) {
        return newsResource.findAllByOrderByPublishDateDesc(PageRequest.of(offset, limit)).stream()
                .map(articleEntity -> modelMapper.map(articleEntity, ArticleDto.class))
                .toList();
    }

    private void saveNewsInDB(NewsApiResponse response) {
        List<ArticleEntity> articlesToSave =  response.articles().stream().map(this::mapArticle).toList();
        //In real world, here should be considered check for duplicates
        newsResource.saveAll(articlesToSave);
    }

    private boolean moreNewsToBeSaved(NewsApiRequest request, NewsApiResponse response) {
        return request.getPage() <= response.totalResults() / request.getMaxPageSize()
                && request.getPage() < 2;   //shortening requests nb, newsapi.org has daily limit!
    }

    // modelMapper does not support new java Record, need to "map" manually
    private ArticleEntity mapArticle(Article article) {
        return new ArticleEntity(article.title(), article.description(), article.publishedAt());
    }
}
