package com.mserwanski.testapp.news.model;

import java.util.Date;

public record Article(Source source, String author, String title, String description, Date publishedAt) {
    
}
