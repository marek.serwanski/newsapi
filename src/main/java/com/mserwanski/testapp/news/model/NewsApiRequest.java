package com.mserwanski.testapp.news.model;

import lombok.Getter;
import lombok.Setter;

// Here are only few of available params, with default values for testing purposes, more can be found: https://newsapi.org/docs/endpoints/everything
@Setter
@Getter
public class NewsApiRequest {

    private static final int DEFAULT_PAGE_SIZE = 100;

    private String query;
    private int maxPageSize;
    private int page;

    public NewsApiRequest() {
        this.maxPageSize = DEFAULT_PAGE_SIZE;
        this.page = 1;
    }

    public NewsApiRequest increasedPageRequest() {
        this.page++;
        return this;
    }
}
