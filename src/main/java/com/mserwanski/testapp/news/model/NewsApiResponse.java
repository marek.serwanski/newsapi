package com.mserwanski.testapp.news.model;

import java.util.List;

public record NewsApiResponse(String status, Integer totalResults, List<Article> articles) {
}
