package com.mserwanski.testapp.news.model;

public record Source(String id, String name) {
}
