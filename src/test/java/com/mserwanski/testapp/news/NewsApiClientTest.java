package com.mserwanski.testapp.news;

import com.mserwanski.testapp.news.model.NewsApiRequest;
import com.mserwanski.testapp.news.model.NewsApiResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static com.mserwanski.testapp.news.NewsTestFactory.TEST_CLIENT_KEY;
import static com.mserwanski.testapp.news.NewsTestFactory.TEST_CLIENT_URL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NewsApiClientTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private NewsApiClient newsApiClient;

    @BeforeEach
    void setup() {
        newsApiClient.setKey(TEST_CLIENT_KEY);
        newsApiClient.setUrl(TEST_CLIENT_URL);
    }

    @Test
    void shouldReturnResponse() {
        NewsApiRequest request = new NewsApiRequest();
        when(restTemplate.getForObject(NewsTestFactory.newsApiTestUrl(), NewsApiResponse.class))
                .thenReturn(NewsTestFactory.testOkNewsApiResponse());

        NewsApiResponse response = newsApiClient.getNewsApiResponce(request);

        assertThat(response.status()).isEqualTo("ok");
    }

    @Test
    void shouldNotSaveNewsAfterIncompleteResponse() {
        when(restTemplate.getForObject(NewsTestFactory.newsApiTestUrl(), NewsApiResponse.class))
                .thenReturn(NewsTestFactory.testIncompleteNewsApiResponse());

        assertThatThrownBy(() -> newsApiClient.getNewsApiResponce(new NewsApiRequest())).isInstanceOf(AssertionError.class);
    }

    @Test
    void shouldNotSaveNewsAfterErrorResponse() {
        when(restTemplate.getForObject(NewsTestFactory.newsApiTestUrl(), NewsApiResponse.class))
                .thenReturn(NewsTestFactory.testErrorNewsApiResponse());

        assertThatThrownBy(() -> newsApiClient.getNewsApiResponce(new NewsApiRequest())).isInstanceOf(RestClientException.class);
    }

}