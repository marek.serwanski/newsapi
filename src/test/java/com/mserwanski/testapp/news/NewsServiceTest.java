package com.mserwanski.testapp.news;

import com.mserwanski.testapp.news.model.NewsApiRequest;
import com.mserwanski.testapp.news.model.NewsApiResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.List;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class NewsServiceTest {

    @Mock
    private NewsApiClient newsApiClient;
    @Mock
    private NewsResource newsResource;

    private NewsService newsService;

    @BeforeEach
    void setup() {
        newsService = new NewsService(newsApiClient, newsResource, new ModelMapper());
    }

    @Test
    void shouldSaveSimpleNews() {
        NewsApiRequest request = new NewsApiRequest();
        NewsApiResponse response = NewsTestFactory.testOkNewsApiResponse();
        when(newsApiClient.getNewsApiResponce(request)).thenReturn(response);

        newsService.saveNewsFromNewsApi(request);

        ArgumentCaptor<List<ArticleEntity>> articlesCaptor = forClass(List.class);
        verify(newsResource).saveAll(articlesCaptor.capture());
        List<ArticleEntity> savedArticles = articlesCaptor.getValue();

        assertThat(savedArticles.size()).isEqualTo(response.articles().size());
        assertThat(savedArticles.get(0).getTitle()).isEqualTo(response.articles().get(0).title());
        assertThat(savedArticles.get(1).getDescription()).isEqualTo(response.articles().get(1).description());
    }

    @Test
    void shouldSaveBatchNews() {
        NewsApiRequest request = new NewsApiRequest();
        request.setMaxPageSize(1);
        when(newsApiClient.getNewsApiResponce(request)).thenReturn(NewsTestFactory.testOkNewsApiResponse());

        newsService.saveNewsFromNewsApi(request);

        verify(newsResource, times(2)).saveAll(any());
    }

    @Test
    void shouldGetNews() {
        when(newsResource.findAllByOrderByPublishDateDesc(any(PageRequest.class))).thenReturn(NewsTestFactory.testNewsPageResult());

        List<ArticleDto> result = newsService.getNews(0,5);

        assertThat(result.size()).isEqualTo(2);
        assertThat(result.get(0).getTitle()).isEqualTo(NewsTestFactory.TEST_TITLE1);
        assertThat(result.get(1).getDescription()).isEqualTo(NewsTestFactory.TEST_DESC2);
    }

    @Test
    void shouldNotCrashIfNoNews() {
        when(newsResource.findAllByOrderByPublishDateDesc(any(PageRequest.class))).thenReturn(new PageImpl<>(emptyList()));
        newsService.getNews(0,5);
    }
}