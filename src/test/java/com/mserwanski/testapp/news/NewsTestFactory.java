package com.mserwanski.testapp.news;

import com.mserwanski.testapp.news.model.Article;
import com.mserwanski.testapp.news.model.NewsApiResponse;
import com.mserwanski.testapp.news.model.Source;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.Date;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

class NewsTestFactory {

    static final String TEST_CLIENT_KEY = "testKey";
    static final String TEST_CLIENT_URL = "http://dummy.org?key=%s";
    static final String TEST_TITLE1 = "Title1";
    static final String TEST_TITLE2 = "Title2";
    static final String TEST_DESC1 = "Desc1";
    static final String TEST_DESC2 = "Desc2";
    static final String TEST_AUTHOR1 = "Author1";
    static final String TEST_AUTHOR2 = "Author2";
    static final String TEST_ARTICLE1 = "Article1";
    static final String TEST_ARTICLE2 = "Article2";


    static NewsApiResponse testOkNewsApiResponse() {
        List<Article> articles = asList(
                new Article(new Source("1", TEST_ARTICLE1), TEST_AUTHOR1, TEST_TITLE1, TEST_DESC1, new Date()),
                new Article(new Source("2", TEST_ARTICLE2), TEST_AUTHOR2, TEST_TITLE2, TEST_DESC2, new Date())
        );
        return new NewsApiResponse("ok", 2, articles);
    }

    static NewsApiResponse testIncompleteNewsApiResponse() {
        return new NewsApiResponse("ok", null, null);
    }

    static NewsApiResponse testErrorNewsApiResponse() {
        return new NewsApiResponse("error", 0, emptyList());
    }

    static String newsApiTestUrl() {
        return String.format(TEST_CLIENT_URL, TEST_CLIENT_KEY);
    }

    static Page<ArticleEntity> testNewsPageResult() {
        return new PageImpl<>(asList(
                new ArticleEntity(TEST_TITLE1, TEST_DESC1, new Date()),
                new ArticleEntity(TEST_TITLE2, TEST_DESC2, new Date())
        ));
    }
}
